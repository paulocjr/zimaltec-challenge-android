package com.zimaltec.challenge

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.intent.rule.IntentsTestRule
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.filters.LargeTest
import com.zimaltec.challenge.view.main.MainActivity
import org.hamcrest.Matchers.not
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

/**
 * Created by paulo on 27-10-2019.
 */
@RunWith(JUnit4::class)
@LargeTest
class MainActivityInstrumentedTest {

    @Rule
    @JvmField
    val rule = IntentsTestRule(MainActivity::class.java)

    @Test
    fun checkViewIsDisplayed() {
        onView(withId(R.id.navigation)).check(matches(isDisplayed()))
        onView(withId(R.id.content_main)).check(matches(isDisplayed()))
        onView(withId(R.id.frame_container)).check(matches(isDisplayed()))
    }

    @Test
    fun checkViewIsNotDisplayed() {
        onView(withId(R.id.layoutContentError)).check(matches(not(isDisplayed())))
    }
}