package com.zimaltec.challenge.repository.film

import androidx.lifecycle.LiveData
import com.zimaltec.challenge.models.BaseData
import com.zimaltec.challenge.models.FilmModel
import com.zimaltec.challenge.service.models.Film
import rx.Observable

/**
 * Created by paulo on 22-10-2019.
 */
interface IFilmRepository {

    /**
     * Retrieves all films
     */
    fun getAllFilms() : LiveData<BaseData<List<Film>>>

    /**
     * Get the film by identifier
     */
    fun getFilmById(id: Int) : Observable<FilmModel>

}