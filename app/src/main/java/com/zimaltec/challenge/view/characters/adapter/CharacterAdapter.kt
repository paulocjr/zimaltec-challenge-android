package com.zimaltec.challenge.view.characters.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.zimaltec.challenge.databinding.LayoutItemCharacterBinding
import com.zimaltec.challenge.service.models.Character
import com.zimaltec.challenge.viewmodel.character.CharacterViewModel

/**
 * Created by pcamilo on 24/10/2019
 */
class CharacterAdapter(viewModel: CharacterViewModel) :
    RecyclerView.Adapter<CharacterAdapter.CharacterViewHolder>() {

    private var mCharacterList = arrayListOf<Character>()
    private var mCharacterViewModel: CharacterViewModel = viewModel

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CharacterViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = LayoutItemCharacterBinding.inflate(inflater)
        return CharacterViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return mCharacterList.size
    }

    override fun onBindViewHolder(holder: CharacterViewHolder, position: Int) {
        holder.bind(mCharacterList[position])
    }

    // view holder for layout = layout_item_character.xml
    inner class CharacterViewHolder(private val binding: LayoutItemCharacterBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(item: Character) {
            binding.model = item

            binding.mainContainer.setOnClickListener {
                mCharacterViewModel.mCharacterSelected.value = item
            }

            binding.executePendingBindings()
        }
    }

    /**
     * Set all data in layout item
     *
     * @param data the data with elements
     */
    fun submitList(data: List<Character>) {
        this.mCharacterList = data as ArrayList<Character>
        notifyDataSetChanged()
    }

}