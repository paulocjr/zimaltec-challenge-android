package com.zimaltec.challenge.di.app

import android.app.Application
import com.zimaltec.challenge.ZimaltecApplication
import com.zimaltec.challenge.di.builder.ActivityBuilder
import com.zimaltec.challenge.di.builder.FragmentBuilder
import com.zimaltec.challenge.di.builder.ViewModelBuilder
import dagger.BindsInstance
import dagger.Component
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

/**
 * Created by paulo on 22-10-2019.
 */
@Singleton
@Component(
    modules = [
        AppModule::class,
        ViewModelBuilder::class,
        FragmentBuilder::class,
        ActivityBuilder::class,
        AndroidSupportInjectionModule::class])
interface AppComponent {

    fun inject(application: ZimaltecApplication)

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun application(application: Application): Builder

        fun build(): AppComponent
    }
}