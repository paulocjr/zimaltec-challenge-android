package com.zimaltec.challenge.models

/**
 * Created by paulo on 22-10-2019.
 */
class BaseData<T>(var success: Boolean, var data: T?)