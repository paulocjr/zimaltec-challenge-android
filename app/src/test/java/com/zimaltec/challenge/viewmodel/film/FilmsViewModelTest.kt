package com.zimaltec.challenge.viewmodel.film

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.MutableLiveData
import com.jraska.livedata.TestObserver
import com.nhaarman.mockitokotlin2.doReturn
import com.zimaltec.challenge.models.BaseData
import com.zimaltec.challenge.models.FilmModel
import com.zimaltec.challenge.repository.film.FilmRepository
import com.zimaltec.challenge.service.models.Film
import junit.framework.TestCase
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import rx.Observable

/**
 * Created by paulo on 27-10-2019.
 */
@RunWith(JUnit4::class)
class FilmsViewModelTest {

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    @Mock
    lateinit var filmRepository: FilmRepository

    lateinit var filmViewModel: FilmsViewModel

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        this.filmViewModel = FilmsViewModel(this.filmRepository)
    }

    @Test
    fun `Test get all films with data NOT NULL`() {
        // Given
        val data = MutableLiveData<BaseData<List<Film>>>()
        data.value = BaseData(true, arrayListOf())

        Mockito.`when`(filmRepository.getAllFilms()).thenReturn(data)
        val value = TestObserver.test(filmViewModel.getAllFilms()).value()

        // Then
        TestCase.assertTrue(value.success)
    }

    @Test
    fun `Test get all films with data NULL`() {
        // Given
        val data = MutableLiveData<BaseData<List<Film>>>()
        data.value = BaseData(false, emptyList())

        Mockito.`when`(filmRepository.getAllFilms()).thenReturn(data)
        val value = TestObserver.test(filmViewModel.getAllFilms()).value()

        // Then
        TestCase.assertFalse(value.success)
    }

    @Test
    fun `Test get film by identifier with data NOT NULL`() {
        // Given
        Mockito.`when`(filmRepository.getFilmById(1)).doReturn(Observable.just(getFilmModel()))
        TestObserver.test(filmViewModel.getFilmById(1)).value()
    }

    private fun getFilmModel() = FilmModel("", "", arrayListOf(), "")

}