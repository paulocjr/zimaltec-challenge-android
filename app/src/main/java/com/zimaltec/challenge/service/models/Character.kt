package com.zimaltec.challenge.service.models

import androidx.databinding.BaseObservable
import com.google.gson.annotations.SerializedName
import java.io.Serializable

/**
 * Created by paulo on 22-10-2019.
 */
class Character(@SerializedName("url") var url: String,
                @SerializedName("name") var name : String,
                @SerializedName("gender") var gender: String,
                @SerializedName("birth_year") var birthYear: String): BaseObservable(), Serializable {

    companion object {
        val EXTRA_PARAM = "CharacterParam"
    }
}