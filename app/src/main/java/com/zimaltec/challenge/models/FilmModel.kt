package com.zimaltec.challenge.models

import androidx.databinding.BaseObservable
import com.zimaltec.challenge.service.models.Character
import java.io.Serializable

/**
 * Created by paulo on 25-10-2019.
 */
data class FilmModel (val title : String, val releaseDate : String, val characters : MutableList<Character>, var url: String) : BaseObservable(), Serializable