package com.zimaltec.challenge.di.builder

import com.zimaltec.challenge.di.scope.Activity
import com.zimaltec.challenge.view.characters.detail.CharacterDetailActivity
import com.zimaltec.challenge.view.films.detail.FilmDetailActivity
import com.zimaltec.challenge.view.main.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * Created by pcamilo on 23/10/2019
 */
@Module
abstract class ActivityBuilder {

    @Activity
    @ContributesAndroidInjector
    abstract fun bindMainActivity(): MainActivity


    @Activity
    @ContributesAndroidInjector
    abstract fun bindCharacterDetailActivity(): CharacterDetailActivity

    @Activity
    @ContributesAndroidInjector
    abstract fun bindFilmDetailActivity(): FilmDetailActivity
}