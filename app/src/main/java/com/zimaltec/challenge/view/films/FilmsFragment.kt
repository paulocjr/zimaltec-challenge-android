package com.zimaltec.challenge.view.films


import android.content.Intent
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.zimaltec.challenge.R
import com.zimaltec.challenge.databinding.FragmentFilmsBinding
import com.zimaltec.challenge.service.models.Film
import com.zimaltec.challenge.view.base.BaseFragment
import com.zimaltec.challenge.view.films.detail.FilmDetailActivity
import com.zimaltec.challenge.viewmodel.film.FilmsViewModel

/**
 * A simple [Fragment] subclass.
 *
 */
class FilmsFragment : BaseFragment<FragmentFilmsBinding, FilmsViewModel>(R.layout.fragment_films) {

    override fun getViewModelClass(): Class<FilmsViewModel> = FilmsViewModel::class.java

    override fun initBinding() {
        getBinding().apply {
            lifecycleOwner = this@FilmsFragment
            viewModel = getViewModelFragment()
        }

        getAllFilms()

        getViewModelFragment().mFilmSelected.observe(this, Observer {
            onFilmClicked(it)
        })
    }

    /**
     * Item selected on Adapter
     */
    private fun onFilmClicked(film: Film) {
        val intentFilmDetail = Intent(requireActivity(), FilmDetailActivity::class.java)
        intentFilmDetail.putExtra(Film.EXTRA_PARAM, film)
        startActivity(intentFilmDetail)
    }

    private fun getAllFilms() {
        getViewModelFragment().getAllFilms().observe(this, Observer { res ->
            if (res != null && res.success) {
                getBinding().rvFilms.visibility = View.VISIBLE
                getViewModelFragment().setFilmInAdapter(res.data!!)
            } else {

            }
        })
    }

}
