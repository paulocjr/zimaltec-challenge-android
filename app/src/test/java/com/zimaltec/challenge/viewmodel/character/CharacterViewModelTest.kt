package com.zimaltec.challenge.viewmodel.character

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.MutableLiveData
import com.jraska.livedata.TestObserver
import com.zimaltec.challenge.models.BaseData
import com.zimaltec.challenge.repository.character.CharacterRepository
import org.junit.Test
import com.zimaltec.challenge.service.models.Character
import junit.framework.TestCase.assertFalse
import junit.framework.TestCase.assertTrue

import org.junit.Before
import org.junit.Rule
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

/**
 * Created by paulo on 27-10-2019.
 */
@RunWith(JUnit4::class)
class CharacterViewModelTest {

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    @Mock
    lateinit var characterRepository: CharacterRepository

    lateinit var characterViewModel: CharacterViewModel

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        this.characterViewModel = CharacterViewModel(this.characterRepository)
    }

    @Test
    fun `Test get all characters with data NOT NULL`() {
        // Given
        val data = MutableLiveData<BaseData<List<Character>>>()
        data.value = BaseData(true, arrayListOf())

        Mockito.`when`(characterRepository.getAllCharacters()).thenReturn(data)
        val value = TestObserver.test(characterViewModel.getAllCharacters()).value()

        // Then
        assertTrue(value.success)
    }

    @Test
    fun `Test get all characters with data NULL`() {
        // Given
        val data = MutableLiveData<BaseData<List<Character>>>()
        data.value = BaseData(false, emptyList())

        Mockito.`when`(characterRepository.getAllCharacters()).thenReturn(data)
        val value = TestObserver.test(characterViewModel.getAllCharacters()).value()

        // Then
        assertFalse(value.success)
    }
}