package com.zimaltec.challenge.view.splash

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import com.zimaltec.challenge.R
import com.zimaltec.challenge.view.main.MainActivity

class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
    }

    override fun onResume() {
        super.onResume()
        Handler().postDelayed({
            startLoginActivity()
        }, 2000)
    }

    /**
     * Start Login activity
     */
    private fun startLoginActivity() {
        startActivity(Intent(this, MainActivity::class.java))
        overridePendingTransition(R.anim.anim_fade_in, R.anim.anim_fade_out)
        finish()
    }
}
