package com.zimaltec.challenge.repository.character

import androidx.lifecycle.LiveData
import com.zimaltec.challenge.models.BaseData
import com.zimaltec.challenge.service.models.Character

/**
 * Created by paulo on 22-10-2019.
 */
interface ICharacterRepository {

    /**
     * Retrieves all character
     */
    fun getAllCharacters() : LiveData<BaseData<List<Character>>>

}