package com.zimaltec.challenge.view.films.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.zimaltec.challenge.databinding.LayoutItemCharacterBinding
import com.zimaltec.challenge.service.models.Character
import com.zimaltec.challenge.viewmodel.film.FilmsViewModel


/**
 * Created by paulo on 26-10-2019.
 */
class FilmCharacterAdapter(viewModel: FilmsViewModel) : RecyclerView.Adapter<FilmCharacterAdapter.FilmCharacterViewHolder>() {

    private var mCharacterList = arrayListOf<Character>()
    private var mFilmViewModel: FilmsViewModel = viewModel

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FilmCharacterViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = LayoutItemCharacterBinding.inflate(inflater)
        return FilmCharacterViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return mCharacterList.size
    }

    override fun onBindViewHolder(holder: FilmCharacterViewHolder, position: Int) {
        holder.bind(mCharacterList[position])
    }

    // view holder for layout = layout_item_character.xml
    inner class FilmCharacterViewHolder(private val binding: LayoutItemCharacterBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(item: Character) {
            binding.model = item

            binding.mainContainer.setOnClickListener {
                mFilmViewModel.mCharacterSelected.value = item
            }

            binding.executePendingBindings()
        }
    }

    /**
     * Set all data in layout item
     *
     * @param data the data with elements
     */
    fun submitList(data: List<Character>) {
        this.mCharacterList = data as ArrayList<Character>
        notifyDataSetChanged()
    }

}