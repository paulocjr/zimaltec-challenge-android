package com.zimaltec.challenge.service.film

import com.zimaltec.challenge.service.models.BaseResponse
import com.zimaltec.challenge.service.models.Film
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import rx.Observable

/**
 * Created by paulo on 22-10-2019.
 */
interface FilmService {

    /**
     * Retrieves all films
     */
    @GET("films")
    fun getAllFilms(): Call<BaseResponse<List<Film>>>

    /**
     * Get the film by identifier
     */
    @GET("films/{id}")
    fun getFilmById(@Path("id") id: Int) : Observable<Film>
}