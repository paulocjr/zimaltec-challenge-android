package com.zimaltec.challenge.service.models

import com.google.gson.annotations.SerializedName

/**
 * Created by pcamilo on 23/10/2019
 */
class BaseResponse<T>(
        @SerializedName("count") val count: Int,
        @SerializedName("next") val next: String,
        @SerializedName("previous") val previous: String,
        @SerializedName("results") val results: T)