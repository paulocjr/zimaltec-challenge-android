package com.zimaltec.challenge.viewmodel.character

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.zimaltec.challenge.models.BaseData
import com.zimaltec.challenge.repository.character.CharacterRepository
import com.zimaltec.challenge.service.models.Character
import com.zimaltec.challenge.view.characters.adapter.CharacterAdapter
import com.zimaltec.challenge.viewmodel.base.BaseViewModel
import javax.inject.Inject

/**
 * Created by paulo on 22-10-2019.
 */
class CharacterViewModel @Inject constructor(characterRepository: CharacterRepository) : BaseViewModel() {

    private val repository: CharacterRepository = characterRepository
    private lateinit var mCharacterAdapter: CharacterAdapter
    var mCharacterSelected = MutableLiveData<Character>()

    fun getAllCharacters(): LiveData<BaseData<List<Character>>> {
        showLoading()

        val mData = MutableLiveData<BaseData<List<Character>>>()

        return Transformations.map(repository.getAllCharacters()) { data ->
            data.let {

                if (it.success && it.data != null) {
                    mData.value = BaseData(true, it.data)
                } else {
                    mData.value = BaseData(false, null)
                }

                hideLoading()

                mData.value
            }
        }
    }

    /**
     * Set values in adapter
     */
    fun setCharacterInAdapter(data: List<Character>){
        mCharacterAdapter.submitList(data)
    }

    /**
     * Get adapter to set in Recycle View
     */
    fun getAdapter() : CharacterAdapter{
        mCharacterAdapter = CharacterAdapter( this)
        return mCharacterAdapter
    }
}