package com.zimaltec.challenge.viewmodel.film

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.zimaltec.challenge.models.BaseData
import com.zimaltec.challenge.models.FilmModel
import com.zimaltec.challenge.repository.film.FilmRepository
import com.zimaltec.challenge.service.models.Character
import com.zimaltec.challenge.service.models.Film
import com.zimaltec.challenge.view.films.adapter.FilmAdapter
import com.zimaltec.challenge.view.films.adapter.FilmCharacterAdapter
import com.zimaltec.challenge.viewmodel.base.BaseViewModel
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import javax.inject.Inject

/**
 * Created by paulo on 22-10-2019.
 */
class FilmsViewModel @Inject constructor(filmRepository: FilmRepository) : BaseViewModel() {

    private val repository: FilmRepository = filmRepository
    private lateinit var mFilmAdapter: FilmAdapter
    private lateinit var mFilmCharacterAdapter: FilmCharacterAdapter
    var mFilmSelected = MutableLiveData<Film>()
    var mCharacterSelected = MutableLiveData<Character>()

    fun getAllFilms() : LiveData<BaseData<List<Film>>> {
        showLoading()

        val mData = MutableLiveData<BaseData<List<Film>>>()

        return Transformations.map(repository.getAllFilms()) { data ->
            data.let {

                if (it.success && it.data != null) {
                    mData.value = BaseData(true, it.data)
                } else {
                    mData.value = BaseData(false, null)
                }

                hideLoading()

                mData.value
            }
        }
    }

    fun getFilmById(filmId: Int): MutableLiveData<BaseData<FilmModel>> {
        showLoading()
        val mData = MutableLiveData<BaseData<FilmModel>>()

        this.repository.getFilmById(filmId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                mData.value =  BaseData(true, it)
            },{
                mData.value =  BaseData(false, null)
            },{
            })

        return mData
    }

    /**
     * Set values in adapter
     */
    fun setFilmInAdapter(data: List<Film>){
        mFilmAdapter.submitList(data)
    }

    /**
     * Get adapter to set in Recycle View
     */
    fun getAdapter() : FilmAdapter{
        mFilmAdapter = FilmAdapter( this)
        return mFilmAdapter
    }

    /**
     * Get adapter to set in Recycle View
     */
    fun getFilmCharaterAdapter() : FilmCharacterAdapter {
        mFilmCharacterAdapter = FilmCharacterAdapter( this)
        return mFilmCharacterAdapter
    }

    /**
     * Set values in adapter
     */
    fun setCharacterInAdapter(data: List<Character>){
        mFilmCharacterAdapter.submitList(data)
    }

}