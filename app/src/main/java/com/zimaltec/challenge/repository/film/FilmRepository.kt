package com.zimaltec.challenge.repository.film

import android.net.Uri
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.zimaltec.challenge.models.BaseData
import com.zimaltec.challenge.models.FilmModel
import com.zimaltec.challenge.service.APIClient
import com.zimaltec.challenge.service.character.CharacterService
import com.zimaltec.challenge.service.film.FilmService
import com.zimaltec.challenge.service.models.BaseResponse
import com.zimaltec.challenge.service.models.Character
import com.zimaltec.challenge.service.models.Film
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import rx.Observable
import javax.inject.Inject

/**
 * Created by paulo on 22-10-2019.
 */
class FilmRepository @Inject constructor (apiClient: APIClient) : IFilmRepository {

    private val TAG = FilmRepository::class.java.simpleName
    private val mFilmService: FilmService = apiClient.getRetrofit().create(FilmService::class.java)
    private val mCharacterService: CharacterService = apiClient.getRetrofit().create(CharacterService::class.java)

    override fun getAllFilms(): LiveData<BaseData<List<Film>>> {
        val data: MutableLiveData<BaseData<List<Film>>> = MutableLiveData()

        mFilmService.getAllFilms()
            .enqueue(object : Callback<BaseResponse<List<Film>>> {

                override fun onResponse(call: Call<BaseResponse<List<Film>>>, response: Response<BaseResponse<List<Film>>>) {
                    if (response.isSuccessful) {
                        if (response.body() != null && response.body()?.results != null) {
                            data.value = BaseData(true, response.body()!!.results)
                        } else {
                            data.value = BaseData(false, null)
                        }
                    } else {
                        data.value = BaseData(false, null)
                        Log.e(TAG, response.message())
                    }
                }

                override fun onFailure(call: Call<BaseResponse<List<Film>>>, t: Throwable) {
                    data.value = BaseData(false, null)
                    t.message.let {
                        Log.e(TAG, it.toString())
                    }
                }
            })

        return data
    }

    override fun getFilmById(id: Int): Observable<FilmModel> {
        return mFilmService.getFilmById(id)
            .flatMap { film ->

                Observable.zip(
                    Observable.just(FilmModel(film.title, film.releaseDate, ArrayList(), film.url)),
                    Observable.from(film.characters)

                        .flatMap {
                                characterUrl ->
                            mCharacterService.getCharacterById(Uri.parse(characterUrl).lastPathSegment)
                        }

                        .flatMap {
                                res ->
                            Observable.just(Character(res.url, res.name, res.gender, res.birthYear))
                        }.toList()

                ) { filmRes, characters ->
                    filmRes.characters.addAll(characters)
                    filmRes
                }
            }
    }


}