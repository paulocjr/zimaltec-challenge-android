package com.zimaltec.challenge.view.characters.detail

import android.view.MenuItem
import com.zimaltec.challenge.R
import com.zimaltec.challenge.databinding.ActivityCharacterDetailBinding
import com.zimaltec.challenge.service.models.Character
import com.zimaltec.challenge.view.base.BaseActivity
import com.zimaltec.challenge.viewmodel.character.CharacterViewModel
import kotlinx.android.synthetic.main.activity_character_detail.*

class CharacterDetailActivity : BaseActivity<ActivityCharacterDetailBinding, CharacterViewModel>(R.layout.activity_character_detail) {

    override fun getViewModelClass(): Class<CharacterViewModel> = CharacterViewModel::class.java

    lateinit var mCharacter: Character

    override fun initBinding() {
        setSupportActionBar(toolbar)
        getBinding().apply {
            lifecycleOwner = this@CharacterDetailActivity
        }

        receiveDataFromBundle()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    private fun receiveDataFromBundle() {
        val bundle = intent.extras
        if (bundle == null || !bundle.containsKey(Character.EXTRA_PARAM)) {
            backHome()
        } else {
            mCharacter = bundle.getSerializable(Character.EXTRA_PARAM) as Character
            getBinding().model = mCharacter
            getBinding().contentLayout.model = mCharacter
            setupToolbarTitle(mCharacter.name, true)
        }
    }

    private fun backHome() {
        finish()
    }
}
