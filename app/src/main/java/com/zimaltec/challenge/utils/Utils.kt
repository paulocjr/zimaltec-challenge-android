package com.zimaltec.challenge.utils

import android.annotation.SuppressLint
import java.text.SimpleDateFormat
import java.util.regex.Pattern


/**
 * Created by paulo on 22-10-2019.
 */
class Utils {

    companion object {

        /**
         * Method to format the date
         */
        @SuppressLint("SimpleDateFormat")
        fun formatDate(value: String): String {
            try {
                val format = SimpleDateFormat("yyyy-MM-dd")
                val date = format.parse(value)
                val formatted = SimpleDateFormat("yyyy")
                return formatted.format(date)
            } catch (e: Exception) {
                e.printStackTrace()
            }
            return ""
        }

        /**
         * Get the image path of Film
         */
        @JvmStatic
        fun getImage(url : String):String{
            return "https://starwars-visualguide.com/assets/img/"+ splitString(url) +".jpg"
        }

        /**
         * Get the image path of Characters
         */
        @JvmStatic
        fun getImagePeople(url : String):String{
            return "https://starwars-visualguide.com/assets/img/"+ splitPeopleString(url) +".jpg"
        }

        @JvmStatic
        private fun splitString(s: String): String {

            val separated = s.split("/")
            return separated[separated.size - 3]+"/"+separated[separated.size- 2]
        }

        @JvmStatic
        private fun splitPeopleString(s: String): String {

            val separated = s.split("/")
            return "characters/"+separated[separated.size- 2]
        }

        @JvmStatic
         fun extractId(value: String): Int {
            val p = Pattern.compile("(\\d+)")
            val m = p.matcher(value)

            if (m.find()) {
                return m.group().toString().toInt()
            }

            return 0
        }
    }
}