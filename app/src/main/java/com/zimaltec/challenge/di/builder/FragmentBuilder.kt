package com.zimaltec.challenge.di.builder

import com.zimaltec.challenge.di.scope.Fragment
import com.zimaltec.challenge.view.characters.CharactersFragment
import com.zimaltec.challenge.view.films.FilmsFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * Created by pcamilo on 23/10/2019
 */
@Module
abstract class FragmentBuilder {

    @Fragment
    @ContributesAndroidInjector
    abstract fun  bindCharacterFragment(): CharactersFragment

    @Fragment
    @ContributesAndroidInjector
    abstract fun  bindFilmFragment(): FilmsFragment
}