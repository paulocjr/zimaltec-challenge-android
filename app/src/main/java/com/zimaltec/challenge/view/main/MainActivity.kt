package com.zimaltec.challenge.view.main

import android.annotation.SuppressLint
import android.content.IntentFilter
import android.net.ConnectivityManager
import android.view.View
import androidx.lifecycle.ViewModel
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.snackbar.BaseTransientBottomBar
import com.google.android.material.snackbar.Snackbar
import com.zimaltec.challenge.R
import com.zimaltec.challenge.databinding.ActivityMainBinding
import com.zimaltec.challenge.view.base.BaseActivity
import com.zimaltec.challenge.view.characters.CharactersFragment
import com.zimaltec.challenge.view.films.FilmsFragment
import com.zimaltec.challenge.view.receiver.ConnectivityReceiver

@SuppressLint("Registered")
class MainActivity : BaseActivity<ActivityMainBinding, ViewModel>(R.layout.activity_main) , ConnectivityReceiver.ConnectivityReceiverListener {

    override fun getViewModelClass(): Class<ViewModel> = ViewModel::class.java

    private var mSnackBar: Snackbar? = null

    override fun initBinding() {
        initBroadcastReceiver()

        getBinding().apply {
            lifecycleOwner = this@MainActivity
            getBinding().viewModel = getViewModeActivity()
        }

        setupToolbarTitle(getString(R.string.title_menu_item_people), false)
        setupBottomNavigation()
        setPeopleTab()
    }

    override fun onNetworkConnectionChanged(isConnected: Boolean) {
        if (!isConnected) {
            getBinding().layoutContentError.contentError.visibility = View.VISIBLE
            snackConnection(getString(R.string.warning_no_internet), BaseTransientBottomBar.LENGTH_INDEFINITE)
        } else {
            getBinding().layoutContentError.contentError.visibility = View.GONE

            mSnackBar?.let {
                it.dismiss()
                setCurrentTabSelected()
            }
        }
    }

    private fun setCurrentTabSelected() {
        when(getBinding().navigation.selectedItemId) {
            R.id.navigation_films -> {
                setFilmTab()
            }

            R.id.navigation_people -> {
                setPeopleTab()
            }
        }
    }

    override fun onResume() {
        super.onResume()
        ConnectivityReceiver.connectivityReceiverListener = this
    }

    /**
     * Initialize the BroadcastReceiver to lister internet connection
     */
    private fun initBroadcastReceiver() {
        registerReceiver(ConnectivityReceiver(), IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION))
    }

    /**
     * Setup the bottom navigation view with icons and actions
     */
    private fun setupBottomNavigation() {
        getBinding().navigation.setOnNavigationItemSelectedListener(mOnNavigationListener)
    }

    private val mOnNavigationListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.navigation_films -> {
                setFilmTab()
                return@OnNavigationItemSelectedListener true
            }

            R.id.navigation_people -> {
                setPeopleTab()
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }

    /**
     * Show message no internet connection
     */
    private fun snackConnection(message: String, duration: Int) {
        mSnackBar = Snackbar.make(findViewById(R.id.content_main), message, duration)
        mSnackBar?.show()
    }

    /**
     * Set people tab
     */
    private fun setPeopleTab() {
        setupToolbarTitle(getString(R.string.title_menu_item_people), false)
        loadFragment(CharactersFragment(), R.id.frame_container, CharactersFragment::class.java.canonicalName.toString())
    }

    /**
     * Set film tab
     */
    private fun setFilmTab() {
        setupToolbarTitle(getString(R.string.title_menu_item_films), false)
        loadFragment(FilmsFragment(), R.id.frame_container, FilmsFragment::class.java.canonicalName.toString())
    }

}
