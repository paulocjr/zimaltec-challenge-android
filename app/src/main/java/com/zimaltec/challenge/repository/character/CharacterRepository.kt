package com.zimaltec.challenge.repository.character

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.zimaltec.challenge.models.BaseData
import com.zimaltec.challenge.service.APIClient
import com.zimaltec.challenge.service.character.CharacterService
import com.zimaltec.challenge.service.models.BaseResponse
import com.zimaltec.challenge.service.models.Character
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject

/**
 * Created by paulo on 22-10-2019.
 */
class CharacterRepository @Inject constructor (apiClient: APIClient) : ICharacterRepository {

    private val TAG = CharacterRepository::class.java.simpleName
    private val mCharacterService: CharacterService = apiClient.getRetrofit().create(CharacterService::class.java)

    override fun getAllCharacters(): LiveData<BaseData<List<Character>>> {
        val data: MutableLiveData<BaseData<List<Character>>> = MutableLiveData()

        mCharacterService.getAllCharacters()
            .enqueue(object : Callback<BaseResponse<List<Character>>> {

                override fun onResponse(call: Call<BaseResponse<List<Character>>>, response: Response<BaseResponse<List<Character>>>) {
                    if (response.isSuccessful) {
                        if (response.body() != null && response.body()?.results != null) {
                            data.value = BaseData(true, response.body()!!.results)
                        } else {
                            data.value = BaseData(false, null)
                        }
                    } else {
                        data.value = BaseData(false, null)
                        Log.e(TAG, response.message())
                    }
                }

                override fun onFailure(call: Call<BaseResponse<List<Character>>>, t: Throwable) {
                    data.value = BaseData(false, null)
                    t.message.let {
                        Log.e(TAG, it.toString())
                    }
                }
            })

        return data
    }

}