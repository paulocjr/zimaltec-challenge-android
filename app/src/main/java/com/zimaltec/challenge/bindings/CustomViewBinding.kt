package com.zimaltec.challenge.bindings

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import com.zimaltec.challenge.R

/**
 * Created by paulo on 25-10-2019.
 */
class CustomViewBinding {

    companion object {

        @JvmStatic
        @BindingAdapter("setAdapter")
        fun bindRecyclerViewAdapter(recyclerView: RecyclerView, adapter: RecyclerView.Adapter<*>) {
            recyclerView.setHasFixedSize(true)
            recyclerView.layoutManager = LinearLayoutManager(recyclerView.context)
            recyclerView.adapter = adapter

            if (recyclerView.itemDecorationCount > 0)
                recyclerView.removeItemDecorationAt(0)

            recyclerView.addItemDecoration(DividerItemDecoration(recyclerView.context, LinearLayoutManager.VERTICAL))
        }

        @JvmStatic
        @BindingAdapter("imageUrl")
        fun bindImageView(imageView: ImageView, imageUrl: String) {
            if (imageUrl.isNotEmpty()) {
                Picasso.get()
                    .load(imageUrl)
                    .error(R.drawable.ic_no_picture)
                    .into(imageView)
            }
        }
    }

}