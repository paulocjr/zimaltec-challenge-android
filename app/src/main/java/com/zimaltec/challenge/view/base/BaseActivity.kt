package com.zimaltec.challenge.view.base

import android.os.Bundle
import androidx.annotation.IdRes
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.zimaltec.challenge.R
import dagger.android.AndroidInjection
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import javax.inject.Inject

/**
 * Created by paulo on 22-10-2019.
 */
abstract class BaseActivity<T : ViewDataBinding, V : ViewModel> (@LayoutRes private val layoutResId: Int) : AppCompatActivity(), HasSupportFragmentInjector {

    private lateinit var mDataBinding: T
    private lateinit var mViewModel: V

    @Inject
    lateinit var dispatchingAndroidInjector : DispatchingAndroidInjector<Fragment>

    @Inject
    lateinit var mViewModelFactory: ViewModelProvider.Factory

    @Override
    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        mDataBinding = DataBindingUtil.setContentView(this, layoutResId)

        getViewModelClass().let {
            mViewModel = ViewModelProviders.of(this, mViewModelFactory).get(getViewModelClass())
        }

        initBinding()
    }

    override fun supportFragmentInjector(): AndroidInjector<Fragment> = dispatchingAndroidInjector

    /**
     * Returns the current binding of layout
     *
     * @return the T is a generic type
     */
    fun getBinding(): T = mDataBinding

    /**
     * Returns the current view model
     *
     * @return the T is a generic type
     */
    fun getViewModeActivity(): V = mViewModel

    /**
     * Initialize the binding for layouts in Activities or Fragments
     */
    protected abstract fun initBinding()
    protected abstract fun getViewModelClass(): Class<V>

    /**
     * Load fragment on app
     */
    protected fun loadFragment(fragment: Fragment, @IdRes resourceContainer: Int, name: String) {
        supportFragmentManager.let {
            val transaction = supportFragmentManager.beginTransaction()
            transaction.replace(resourceContainer, fragment, name)
            transaction.setCustomAnimations(R.anim.anim_fade_in, R.anim.anim_fade_out)
            transaction.commit()
        }
    }

    /**
     * Setup the toolbar with title and home back button android native
     */
    protected fun setupToolbarTitle(string: String, homeButtonEnable: Boolean){
        supportActionBar?.let {
            it.title = string
            it.setDisplayShowHomeEnabled(homeButtonEnable)
            it.setDisplayHomeAsUpEnabled(homeButtonEnable)
        }
    }

}