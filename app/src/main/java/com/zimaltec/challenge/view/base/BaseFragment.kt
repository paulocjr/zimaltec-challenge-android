package com.zimaltec.challenge.view.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import dagger.android.support.AndroidSupportInjection
import javax.inject.Inject

/**
 * Created by paulo on 23-10-2019.
 */
abstract class BaseFragment<T : ViewDataBinding, V : ViewModel> (@LayoutRes private val layoutResId: Int) : Fragment() {

    private lateinit var mDataBinding: T
    private lateinit var mViewModel: V
    private lateinit var mParentViewModel: V

    @Inject
    lateinit var mViewModelFactory: ViewModelProvider.Factory

    @Override
    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidSupportInjection.inject(this)
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        mDataBinding = DataBindingUtil.inflate(inflater, layoutResId, container, false)

        getViewModelClass().let {
            mViewModel = ViewModelProviders.of(this, mViewModelFactory).get(getViewModelClass())

            parentFragment?.let {
                mParentViewModel = ViewModelProviders.of(parentFragment!!, mViewModelFactory).get(getViewModelClass())
            }
        }

        initBinding()
        return mDataBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }

    /**
     * Returns the current binding of layout
     *
     * @return the T is a generic type
     */
    fun getBinding(): T = mDataBinding

    /**
     * Returns the current view model
     *
     * @return the T is a generic type
     */
    fun getViewModelFragment(): V = mViewModel

    /**
     * Returns the parent view model
     *
     * @return the T is a generic type
     */
    fun getParentViewModel(): V = mParentViewModel

    protected abstract fun getViewModelClass(): Class<V>

    /**
     * Initialize the binding for layouts in Activities or Fragments
     */
    protected abstract fun initBinding()
}