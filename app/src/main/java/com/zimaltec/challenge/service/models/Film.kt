package com.zimaltec.challenge.service.models

import androidx.databinding.BaseObservable
import com.google.gson.annotations.SerializedName
import java.io.Serializable

/**
 * Created by pcamilo on 23/10/2019
 */
data class Film(@SerializedName("title")val title: String,
           @SerializedName("opening_crawl") val openingCrawl: String,
           @SerializedName("release_date") var releaseDate: String,
           @SerializedName("url") var url: String,
           @SerializedName("characters") val characters: List<String>) : BaseObservable(), Serializable {

    companion object {
        val EXTRA_PARAM = "FilmParam"
    }
}