package com.zimaltec.challenge.view.films.detail

import android.view.MenuItem
import android.view.View
import androidx.lifecycle.Observer
import com.zimaltec.challenge.R
import com.zimaltec.challenge.databinding.ActivityFilmDetailBinding
import com.zimaltec.challenge.service.models.Film
import com.zimaltec.challenge.utils.Utils
import com.zimaltec.challenge.view.base.BaseActivity
import com.zimaltec.challenge.viewmodel.film.FilmsViewModel
import kotlinx.android.synthetic.main.activity_film_detail.*

class FilmDetailActivity : BaseActivity<ActivityFilmDetailBinding, FilmsViewModel>(R.layout.activity_film_detail) {

    override fun getViewModelClass(): Class<FilmsViewModel> = FilmsViewModel::class.java

    lateinit var mFilm: Film

    override fun initBinding() {
        setSupportActionBar(toolbar)
        getBinding().apply {
            lifecycleOwner = this@FilmDetailActivity
            getBinding().viewModel = getViewModeActivity()
        }

        receiveDataFromBundle()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    private fun receiveDataFromBundle() {
        val bundle = intent.extras
        if (bundle == null || !bundle.containsKey(Film.EXTRA_PARAM)) {
            backHome()
        } else {
            mFilm = bundle.getSerializable(Film.EXTRA_PARAM) as Film
            getBinding().model = mFilm
            setupToolbarTitle(mFilm.title, true)

            getFilmWithCharacters(Utils.extractId(mFilm.url))
        }
    }

    private fun getFilmWithCharacters(filmId: Int) {
        getViewModeActivity().getFilmById(filmId).observe(this, Observer { res ->
            getViewModeActivity().hideLoading()

            if (res?.data != null) {
                getBinding().layoutContentError.contentError.visibility = View.GONE
                getBinding().rvCharacters.visibility = View.VISIBLE
                getViewModeActivity().setCharacterInAdapter(res.data!!.characters.toList())
            } else {
                showError()
            }
        })
    }

    private fun showError() {
        getBinding().layoutContentError.contentError.visibility = View.VISIBLE
        getBinding().rvCharacters.visibility = View.GONE
    }

    private fun backHome() {
        finish()
    }
}
