package com.zimaltec.challenge.view.films.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.zimaltec.challenge.databinding.LayoutItemFilmBinding
import com.zimaltec.challenge.service.models.Film
import com.zimaltec.challenge.utils.Utils
import com.zimaltec.challenge.viewmodel.film.FilmsViewModel

/**
 * Created by pcamilo on 24/10/2019
 */
class FilmAdapter (viewModel: FilmsViewModel): RecyclerView.Adapter<FilmAdapter.FilmViewHolder>(){

    private var mFilmList = arrayListOf<Film>()
    private var mFilmViewModel: FilmsViewModel = viewModel

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FilmViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = LayoutItemFilmBinding.inflate(inflater)
        return FilmViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return mFilmList.size
    }

    override fun onBindViewHolder(holder: FilmViewHolder, position: Int) {
        holder.bind(mFilmList[position])
    }

    // view holder for layout = layout_item_film.xml
    inner class FilmViewHolder(private val binding: LayoutItemFilmBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind(item: Film) {
            item.releaseDate = Utils.formatDate(item.releaseDate)
            binding.model = item

            binding.mainContainer.setOnClickListener {
                mFilmViewModel.mFilmSelected.value = item
            }

            binding.executePendingBindings()
        }
    }

    /**
     * Set all data in layout item
     *
     * @param data the data with elements
     */
    fun submitList(data: List<Film>) {
        this.mFilmList = data as ArrayList<Film>
        notifyDataSetChanged()
    }

}