package com.zimaltec.challenge.view.characters


import android.content.Intent
import android.view.View
import androidx.lifecycle.Observer
import com.zimaltec.challenge.R
import com.zimaltec.challenge.databinding.FragmentCharactersBinding
import com.zimaltec.challenge.service.models.Character
import com.zimaltec.challenge.view.base.BaseFragment
import com.zimaltec.challenge.view.characters.detail.CharacterDetailActivity
import com.zimaltec.challenge.viewmodel.character.CharacterViewModel

class CharactersFragment : BaseFragment<FragmentCharactersBinding, CharacterViewModel>(R.layout.fragment_characters) {

    override fun getViewModelClass(): Class<CharacterViewModel> = CharacterViewModel::class.java

    override fun initBinding() {

        getBinding().apply {
            lifecycleOwner = this@CharactersFragment
            viewModel = getViewModelFragment()
        }

        getAllCharacters()

        getViewModelFragment().mCharacterSelected.observe(this, Observer {
            onCharacterClicked(it)
        })
    }

    /**
     * Get all characters list
     */
    private fun getAllCharacters() {
        getViewModelFragment().getAllCharacters().observe(this, Observer { res ->
            if (res != null && res.success) {
                getBinding().rvCharacters.visibility = View.VISIBLE
                getViewModelFragment().setCharacterInAdapter(res.data!!)
            } else {

            }
        })
    }

    /**
     * Item selected on Adapter
     */
    private fun onCharacterClicked(character: Character) {
        val intentFilmDetail = Intent(requireActivity(), CharacterDetailActivity::class.java)
        intentFilmDetail.putExtra(Character.EXTRA_PARAM, character)
        startActivity(intentFilmDetail)
    }
}
