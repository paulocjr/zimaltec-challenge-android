package com.zimaltec.challenge

import android.app.Activity
import android.app.Application
import com.zimaltec.challenge.di.app.DaggerAppComponent
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import javax.inject.Inject

/**
 * Created by paulo on 22-10-2019.
 */
class ZimaltecApplication : Application(), HasActivityInjector {

    @Inject
    lateinit var dispatchingActivityInjector : DispatchingAndroidInjector<Activity>

    override fun onCreate() {
        super.onCreate()
        initInjector()
    }

    override fun activityInjector(): AndroidInjector<Activity> = dispatchingActivityInjector

    /**
     * Initialize the dagger component
     */
    private fun initInjector() {
        DaggerAppComponent.builder()
            .application(this)
            .build()
            .inject(this)
    }
}