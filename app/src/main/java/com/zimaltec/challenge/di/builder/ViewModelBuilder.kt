package com.zimaltec.challenge.di.builder

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.zimaltec.challenge.di.ViewModelKey
import com.zimaltec.challenge.viewmodel.character.CharacterViewModel
import com.zimaltec.challenge.viewmodel.film.FilmsViewModel
import com.zimaltec.challenge.viewmodel.base.ViewModelFactory
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

/**
 * Created by pcamilo on 23/10/2019
 */
@Module
abstract class ViewModelBuilder {

    @Binds
    @IntoMap
    @ViewModelKey(CharacterViewModel::class)
    abstract fun bindCharacterViewModel(characterViewModel: CharacterViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(FilmsViewModel::class)
    abstract fun bindFilmViewModel(filmViewModel: FilmsViewModel): ViewModel

    // ViewModel Factory
    @Binds
    abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory
}