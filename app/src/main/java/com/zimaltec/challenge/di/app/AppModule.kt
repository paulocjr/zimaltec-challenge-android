package com.zimaltec.challenge.di.app

import android.app.Application
import android.content.Context
import com.zimaltec.challenge.service.APIClient
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * Created by paulo on 22-10-2019.
 */
@Module
class AppModule {

    @Provides
    @Singleton
    fun provideContext(app: Application): Context = app.applicationContext

    @Provides
    @Singleton
    fun provideApiClient() = APIClient()
}