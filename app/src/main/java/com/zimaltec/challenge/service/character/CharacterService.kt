package com.zimaltec.challenge.service.character

import com.zimaltec.challenge.service.models.BaseResponse
import com.zimaltec.challenge.service.models.Character
import retrofit2.Call
import retrofit2.http.*
import rx.Observable

/**
 * Created by paulo on 22-10-2019.
 */
interface CharacterService {

    /**
     * Retrieves all character
     */
    @GET("people")
    fun getAllCharacters(): Call<BaseResponse<List<Character>>>


    /**
     * Get the character by identifier
     */
    @GET("people/{id}")
    fun getCharacterById(@Path("id") id: String?) : Observable<Character>

}